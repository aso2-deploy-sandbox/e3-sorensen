# Require statments should only specify the module name
# Note that all modules shall be in lowercase

## Add extra modules here
# require mymodule

# General 
#require iocstats, 3.1.16
#require stream,2.8.10
#require modbus,3.0.0
require essioc 

# Power Supplies 
#require fug,3.1.2
require sorensen,1.1.0
#require caenelsmagnetps
#require sairem
#require tdklambdagenesys

# RF Probe
#require rs_nrpxxsn


# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables
epicsEnvSet("PREFIX", "IOC ISRC LEBT")
epicsEnvSet("DEVICE", "Virtual Machine Installed")
epicsEnvSet("LOCATION", "ESS.ACC_etc")
epicsEnvSet("ENGINEER", "antonisimelio@esss.se")
## Add extra environment variables here

# Load standard module startup scripts
#iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

## Add extra startup scripts requirements here
# iocshLoad("$(module_DIR)/module.iocsh", "MACRO=MACRO_VALUE")

# CALL to the MODULES
############################################################################################################################################

################# ISRC ########################

############################################################
######## FUG @ HCH 15k-100k [High Voltage Power Supply] ######
############################################################
#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=HVPS, IP_addr=172.16.60.57, P=ISrc-010:, R=ISS-HVPS:")
#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=HVPS, IP_addr=127.0.0.1, P=ISrc-010:, R=ISS-HVPS:")
###iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=HVPS, IP_addr=172.16.60.57, P=ISrc-CS:, R=ISS-HVPS-01:,EGU=kV,ASLO=0.1")


############################################################
######## FUG @HCP 35-3500 [Repeller Power Supply] ###########
############################################################
#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=RepPS-01, IP_addr=127.0.0.1, P=ISrc-010:, R=PwrC-RepPS-01:")
###iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=RepPS-01, IP_addr=172.16.60.52, P=ISrc-CS:, R=PwrC-PSRep-01:,EGU=kV,ASLO=0.1")


#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=RepPS-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-RepPS-01:")
###iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=RepPS-02, IP_addr=172.16.60.53, P=LEBT-CS:, R=PwrC-PSRep-01:,EGU=kV,ASLO=0.1")


############################################################
############# TDK Lambda Genesys 10-500 [Coils] ############
############################################################
###iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=CoilsPS-01, IP_ADDR=172.16.60.43, P=ISrc-CS:, R=PwrC-PSCoil-01:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")
###iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=CoilsPS-02, IP_ADDR=172.16.60.44, P=ISrc-CS:, R=PwrC-PSCoil-02:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")
###iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "CONNECTIONNAME=CoilsPS-03, IP_ADDR=172.16.60.45, P=ISrc-CS:, R=PwrC-PSCoil-03:, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")


#iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "connection_name=CoilsPS-01, IP_addr=172.16.60.43, secsub=ISrc-010, disdevidx=PwrC-CoilPS-01")
#iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "connection_name=CoilsPS-02, IP_addr=172.16.60.44, secsub=ISrc-010, disdevidx=PwrC-CoilPS-02")
#iocshLoad("$(tdklambdagenesys_DIR)/tdklambdagenesys.iocsh", "connection_name=CoilsPS-03, IP_addr=172.16.60.45, secsub=ISrc-010, disdevidx=PwrC-CoilPS-03")
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-01, IP_addr=127.0.0.1, secsub=ISrc-010, disdevidx=PwrC-CoilPS-01")
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-02, IP_addr=10.10.1.32, secsub=ISrc-010, disdevidx=PwrC-CoilPS-02")
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-02, IP_addr=127.0.0.1, secsub=ISrc-010, disdevidx=PwrC-CoilPS-02")
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-03, IP_addr=10.10.1.33, secsub=ISrc-010, disdevidx=PwrC-CoilPS-03")
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-03, IP_addr=127.0.0.1, secsub=ISrc-010, disdevidx=PwrC-CoilPS-03")

############################################################
###################### Sairem @ GMP20KED [Magnetron] #########
############################################################
#iocshLoad("$(sairem_DIR)/sairem_Magnetron_RfGen.iocsh", "P=ISrc-010, R=ISS-Magtr, PORT_NAME=Magtr, IP_ADDR=127.0.0.1, SLAVE_ADDR=1, MAX_PWR_W=2000")
#iocshLoad("$(sairem_DIR)/sairem_Magnetron_RfGen.iocsh", "P=ISrc-010, R=ISS-Magtr, PORT_NAME=Magtr, IP_ADDR=10.10.1.38, SLAVE_ADDR=1, MAX_PWR_W=2000")
###iocshLoad("$(sairem_DIR)/sairem_Magnetron_RfGen.iocsh", "P=ISrc-CS:, R=ISS-Magtr-01:, PORT_NAME=Magtr, IP_ADDR=172.16.60.54, SLAVE_ADDR=1, MAX_PWR_W=2000")

################ RF Probes #################################
#iocshLoad("$(rs_nrpxxsn_DIR)/rs_nrpxxsn.iocsh", "P=ISrc-CS:, R=Fwd_RF_Probe:, IP=172.16.60.61, ASYN_PORT=rs_nrpxxsn_1")
#iocshLoad("$(rs_nrpxxsn_DIR)/rs_nrpxxsn.iocsh", "P=ISrc-CS:, R=Ref_RF_Probe:, IP=172.16.60.60, ASYN_PORT=rs_nrpxxsn_2")
############################################################



############################################################
################# Sairem @ AI4S [ATU] ########################
#############################################################
#iocshLoad("$(sairem_DIR)/sairem_ATU_Tuner.iocsh", "P=ISrc-010, R=ISS-ATU, PORT_NAME=ATU, IP_ADDR=127.0.0.1, SLAVE_ADDR=0")
###iocshLoad("$(sairem_DIR)/sairem_ATU_Tuner.iocsh", "P=ISrc-CS:, R=ISS-ATU-01:, PORT_NAME=ATU, IP_ADDR=172.16.60.55, SLAVE_ADDR=0")



################# LEBT ####################################

############################################################
################# Sorensen @ SGA30x501d [SOLENOIDS] ########
############################################################

#### ICSHWI-7513
#iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-01, IP_addr=127.0.0.1, P=LEBT-010, R=PwrC-SolPS-01, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-01, IP_addr=172.16.60.46, P=LEBT-010, R=PwrC-SolPS-01, Amp2Tesla_calib = 8.291E-4")
iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-01, IP_addr=172.16.60.225, P=LEBT-CS:, R=PwrC-PSSol-TEST:, Amp2Tesla_calib = 8.291E-4")

##iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-01, IP_addr=192.168.10.101, P=LEBT-CS:, R=PwrC-PSSol-01:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-02, IP_addr=192.168.10.102, P=LEBT-CS:, R=PwrC-PSSol-02:, Amp2Tesla_calib = 8.291E-4")


############################################################
################# Caen @ Fast-Ps [STEERERS] ########
############################################################
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCH-01:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=172.16.60.48, P=LEBT-010:, R=PwrC-PSCH-01:, Amp2Tesla_calib = 8.291E-4")
###iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=FASTPS_1, IP_ADDR=172.16.60.48, P=LEBT-CS:, R=PwrC-PSCH-01:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

#
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-01, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCV-01:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-01, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCV-01:, Amp2Tesla_calib = 8.291E-4")
###iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=FASTPS_2, IP_ADDR=172.16.60.49, P=LEBT-CS:, R=PwrC-PSCH-02:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")



#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCH-02:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-02, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCH-02:, Amp2Tesla_calib = 8.291E-4")
###iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=FASTPS_3, IP_ADDR=172.16.60.50, P=LEBT-CS:, R=PwrC-PSCV-01:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")



#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCV-02:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-02, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCV-02:, Amp2Tesla_calib = 8.291E-4")
###iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=FASTPS_4, IP_ADDR=172.16.60.51, P=LEBT-CS:, R=PwrC-PSCV-02:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")



## Define Generic Variables For Every Modu
#epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB):$(tdkgen10500_DB):$(sairem_DB):$(sorensen_DB):$(caenelsmagnetps_DB)")
#epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB):$(sairem_DB):$(caenelsmagnetps_DB):$(tdklambdagenesys_DB)")
#epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB):$(tdkgen10500_DB):$(sairem_DB):$(sorensen_DB)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB):$(sairem_DB):$(tdklambdagenesys_DB):$(sorensen_DB):$(caenelsmagnetps_DB):$(rs_nrpxxsn_DB)")
#epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB):$(sairem_DB):$(tdklambdagenesys_DB):$(sorensen_DB):$(caenelsmagnetps_DB)")


#############################################################################################################################################


## Load custom databases
# cd $(E3_IOCSH_TOP)
# dbLoadRecords("db/custom_database1.db", "MACRO1=MACRO1_VALUE,...")
# dbLoadTemplate("db/custom_database2.substitutions", "MACRO1=MACRO1_VALUE,...")
###dbLoadRecords("db/soft_interlock_Rep.template", "P_LPS=ISrc-LPS::, R_LPS=Rep_SIntlk_, P_CS=ISrc-CS:, R_CS=PwrC-PSRep-01:, Interlock_PV=ISrc-LPS::PwrC-PSRep-01_EnCmd")
###dbLoadRecords("db/soft_interlock_Rep.template", "P_LPS=LEBT-LPS::, R_LPS=Rep_SIntlk_, P_CS=LEBT-CS:, R_CS=PwrC-PSRep-01:, Interlock_PV=LEBT-LPS::PwrC-PSRep-01_EnCmd")


# Load Operational Parameters
dbLoadRecords("db/Tunning_Parameters.db")


# Call iocInit to start the IOC
iocInit()

## Add any post-iocInit statements here
## Parameters 
## HVPS

###dbpf ISrc-CS:ISS-HVPS-01:Ramp-S 0.1
###dbpf ISrc-CS:ISS-HVPS-01:RampType-S 1

###dbpf ISrc-CS:PwrC-PSRep-01:Ramp-S 0.1
###dbpf ISrc-CS:PwrC-PSRep-01:RampType-S 1

###dbpf LEBT-CS:PwrC-PSRep-01:Ramp-S 0.1
###dbpf LEBT-CS:PwrC-PSRep-01:RampType-S 1


